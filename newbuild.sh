#!/bin/sh

DOCKER_TAG=mystock:latest

rm -rf data
rm -rf jobs/nohup.out

echo " docker build -f Dockerfile -t ${DOCKER_TAG} ."
docker build -f Dockerfile -t ${DOCKER_TAG} .
echo "#################################################################"

