#!/bin/bash

PWD=`pwd`

if [ -d "${PWD}/data" ]; then
    rm -rf ${PWD}/data/
fi

STOCK_IS_RUN=`docker ps --filter "name=stock" --filter "status=running" | wc -l `
if [ $STOCK_IS_RUN -ge 2 ]; then
    echo "stop & rm stock ..."
    docker stop stock && docker rm stock
fi

STOCK_IS_RUN=`docker ps --filter "name=mysqldb" --filter "status=running" | wc -l `
if [ $STOCK_IS_RUN -ge 2 ]; then
    echo "stop & rm mysqldb ..."
    docker stop mysqldb && docker rm mysqldb
fi
